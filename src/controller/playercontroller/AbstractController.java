package controller.playercontroller;

import java.util.Optional;
import model.attribute.feature.Trait;
import model.character.Character;
import model.player.Player;
import model.question.Question;
import utilities.Utilities;

abstract class AbstractController implements PlayerController {

    private final Player player;
    private boolean hasGuessed;

    AbstractController(final Player player) {
        Utilities.requireNonNull(player);
        this.player = player;
    }

    @Override
    public Character getSelected() {
        return player.getSelected();
    }

    @Override
    public Status getStatus() {
        return hasGuessed ? Status.WON : (player.getRemainingAttempts() == 0 ? Status.LOST : Status.PLAYING);
    }

    @Override
    public abstract void play();

    @Override
    public boolean askPlayer(final Question question) throws InterruptedException {
        Utilities.requireNonNull(question);
        return player.getSelected().has(question.toAttribute());
    }

    @Override
    public void registerAnswer(final Question question, final boolean answer) throws InterruptedException {
        Utilities.requireNonNull(question, answer);
        player.filter(question.toAttribute(), answer);
        if (question.toAttribute().getTrait().equals(Trait.NAME)) {
            if (!answer) {
                player.decreaseAttempts();
            } else {
                hasGuessed = true;
            }
        }
    }

    @Override
    public abstract void endGame(Status status, Optional<Character> opponentCharacter);

    protected Player getPlayer() {
        return player;
    }

}
