package controller.resources;

import java.awt.Image;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.swing.ImageIcon;
import controller.gameoptions.GameSettings;
import controller.gameoptions.Pack;
import model.attribute.Attribute;
import model.attribute.AttributeFactory;
import model.attribute.feature.PhysicalFeature;
import model.attribute.feature.Trait;
import model.character.Character;
import model.character.CharacterBuilder;
import utilities.Utilities;

/**
 * Utility class that allows to load and get the resources for the Game.
 * Note that to properly use this class the "load" method should be called before anything else,
 * any the getter method throws an IllegalStateException if called before the loading of the resources.
 */
public final class Resources {

    private static final String IMAGES_FOLDER = "/images/";
    private static final String IMAGE_EXTENSION = ".png";
    private static final String DEFAULT_IMAGE_NAME = "default";
    private static final int IMAGE_WIDTH_PROPORTION = (int) Math.round(Utilities.getScreenRatio() * 10);

    private static Optional<Image> defaultImage;
    private static Map<Character, Image> res = new HashMap<>();
    private static Optional<Pack> pack = Optional.empty();

    private Resources() {
    }

    /**
     * Allows to get the default Image (i.e. "?").
     * @return the image
     */
    public static Image getDefaultImage() {
        Resources.checkLoaded();
        return defaultImage.get();
    }

    /**
     * Allows to get a Character's Image by giving his/her name.
     * @param name
     *              the name
     * @return the Image
     */
    public static Image getCharacterImage(final String name) {
        Utilities.requireNonNull(name);
        Resources.checkLoaded();
        return res.get(res.keySet().stream().filter(character -> character.getName().equals(name)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("the name does not match any character :" + name)));
    }

    /**
     * Allows to get the Map of all names and Images.
     * @return the map of names and images
     */
    public static Map<String, Image> getNamesAndImages() {
        Resources.checkLoaded();
        final Map<String, Image> out = new HashMap<>();
        res.entrySet().stream().forEach(entry -> out.put(entry.getKey().getName(), entry.getValue()));
        return Collections.unmodifiableMap(out);
    }
    /**
     * Allows to get the full Set of Characters.
     * @return the Set of Character
     */
    public static Set<Character> getCharacters() {
        Resources.checkLoaded();
        return Collections.unmodifiableSet(res.keySet());
    }

    /**
     * Allows to properly load and store resources, Characters pack and images are taken from GameSettings.
     * @throws LoadingException in case of errors during the loading process.
     */
    public static void load() throws LoadingException {
        if (!pack.isPresent() || !pack.get().equals(GameSettings.getPack())) { //loads resources only if settings changed
            pack = Optional.of(GameSettings.getPack());
            res.clear();
            try {
                for (final Character character : CharacterLoader.loadCharacters()) {
                    res.put(character, Resources.loadAndScale(GameSettings.getPack() + "/" + character.getName(), IMAGE_WIDTH_PROPORTION));
                }
                final ImageIcon test = new ImageIcon(res.values().iterator().next()); //random image to get dimension
                defaultImage = Optional.of(new ImageIcon(Resources.class.getResource(IMAGES_FOLDER + DEFAULT_IMAGE_NAME + IMAGE_EXTENSION)).getImage()
                    .getScaledInstance(test.getIconWidth(), test.getIconHeight(), Image.SCALE_SMOOTH));
            } catch (Exception e) {
                throw new LoadingException("Error while loading resources" + System.lineSeparator() + e.getMessage());
            }
        }
    }

    private static void checkLoaded() {
        if (res.isEmpty() || !defaultImage.isPresent()) {
            throw new IllegalStateException("Resources not loaded: call loadResources method in order to get them");
        }
    }

    /*
     * Allows to load and scale an image, given the name of the image and the width proportion.
     */
    private static Image loadAndScale(final String name, final int widthProportion) throws IOException {
        return new ImageIcon(Resources.class.getResource(IMAGES_FOLDER + name + IMAGE_EXTENSION)).getImage()
                .getScaledInstance(Utilities.getScreenDimension().width / widthProportion, -1, Image.SCALE_SMOOTH);
    }

    /**
     * It is used for automatic tests.
     * @return the demo Set of Characters
     */
    public static Set<Character> demoPack() {
        final Attribute rufyHairs = AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.SHORT);
        final Character rufy = new CharacterBuilder().addAll(rufyHairs, AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.MALE), 
                AttributeFactory.from(Trait.HAT), AttributeFactory.from(Trait.NAME, "Rufy")).build();
        final Attribute zoroHairs = AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.GREEN, PhysicalFeature.Length.SHORT);
        final Character zoro = new CharacterBuilder().addAll(zoroHairs, AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.MALE),
                AttributeFactory.from(Trait.NAME, "Zoro")).build();
        final Attribute robinHairs = AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.LONG, PhysicalFeature.HairStyle.STRAIGHT);
        final Character robin = new CharacterBuilder().addAll(robinHairs, AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.FEMALE), 
                AttributeFactory.from(Trait.GLASSES), AttributeFactory.from(Trait.NAME, "Robin")).build();
        return new HashSet<>(Arrays.asList(rufy, zoro, robin));
    }

}
