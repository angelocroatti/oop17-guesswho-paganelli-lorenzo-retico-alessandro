package model.ai;

import java.util.*;
import java.util.stream.Collectors;
import model.attribute.feature.Trait;
import model.player.Player;
import model.question.Question;
import model.question.QuestionFactory;
import utilities.Utilities;

/*
 * Utility class that collects different asking strategies.
 */
final class QuestionStrategy {

    private QuestionStrategy() {
    }

    public static Question randomNonGuessingQuestion(final Player p) {
        Utilities.requireNonNull(p);
        return Utilities.getRandom(QuestionFactory.possibleQuestions(p).stream()
                .filter(q -> !q.toAttribute().getTrait().equals(Trait.NAME)).collect(Collectors.toList()));
    }

    /*
     * Given a Player allows to get the median question to ask.
     * i.e. the one that allows to delete half of the remaining characters (or the most closest number to the half).
     */
    public static Question medianNonGuessingQuestion(final Player p) {
        Utilities.requireNonNull(p);
        final Map<Question, Integer> questionOccurrences = new HashMap<>();
        final Integer halfRemaining = p.getRemaining().size() / 2;
        QuestionFactory.possibleQuestions(p).stream().filter(q -> !q.toAttribute().getTrait().equals(Trait.NAME)).distinct()
            .forEach(q -> questionOccurrences.put(q, QuestionStrategy.countOccurrences(p, q)));
        return questionOccurrences.entrySet().stream() //gets the one with the occurrences most close to the half of remaining characters
            .reduce((e1, e2) -> Math.abs(halfRemaining - e1.getValue()) < Math.abs(halfRemaining - e2.getValue()) ? e1 : e2)
            .orElseThrow(IllegalArgumentException::new).getKey();
    }

    private static Integer countOccurrences(final Player p, final Question q) {
        return (int) QuestionFactory.possibleQuestions(p).stream().filter(question -> question.equals(q)).count();
    }

}
