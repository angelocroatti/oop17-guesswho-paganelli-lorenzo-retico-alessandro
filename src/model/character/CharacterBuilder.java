package model.character;

import java.util.*;
import model.attribute.Attribute;
import model.attribute.feature.Trait;
import utilities.Utilities;

/**
 * Allows to properly build Characters.
 * Note that there is no way to build a Character with no Attributes or no name, in that case an
 * IllegalArgumentException will be thrown.
 */
public class CharacterBuilder {

    private final Set<Attribute> attributes = new HashSet<>();
    private boolean hasBuilt;

    /**
     * Allows to add an Attribute.
     * @throws  IllegalStateException 
     *              in case Character has already been built
     *          IllegalArgumentException 
     *              in case of null argument or
     *              in case you are trying to add an empty name or
     *              in case you are adding an attribute which describes a Trait you already described
     * @param attribute 
     *              to add
     * @return the updated Character Builder
     */
    public CharacterBuilder add(final Attribute attribute) {
        this.check();
        Utilities.requireNonNull(attribute);
        if (attributes.stream().anyMatch(attr -> attr.getTrait().equals(attribute.getTrait()))) {
            throw new IllegalArgumentException("Trait already described: " + attribute.getTrait());
        }
        if (attribute.getTrait().equals(Trait.NAME) && (attribute.getFeatures().isEmpty() 
                || attribute.getFeatures().iterator().next().equals(""))) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
        attributes.add(attribute);
        return this;
    }

    /**
     * Allows to add multiple Attributes.
     * @throws  IllegalStateException 
     *              in case Character has already been built
     *          IllegalArgumentException 
     *              in case of null argument or
     *              in case you are trying to add an empty name or
     *              in case you are adding an attribute which describes a Trait you already described
     * @param attributes 
     *              to add
     * @return the updated Character Builder
     */
    public CharacterBuilder addAll(final Collection<Attribute> attributes) {
        Utilities.requireNonNull(attributes);
        attributes.stream().forEach(this::add);
        return this;
    }

    /**
     * Allows to add multiple Attributes.
     * @throws  IllegalStateException 
     *              in case Character has already been built
     *          IllegalArgumentException 
     *              in case of null argument or
     *              in case you are trying to add an empty name or
     *              in case you are adding an attribute which describes a Trait you already described
     * @param attributes 
     *              to add
     * @return the updated Character Builder
     */
    public CharacterBuilder addAll(final Attribute... attributes) {
        return this.addAll(Arrays.asList(attributes));
    }

    /**
     * Allows to build the Character, it can be called just once.
     * @throws IllegalStateException 
     *              in case Character has already been built  or
     *              in case Name or Attributes have not been set
     * @return the Character
     */
    public Character build() {
        this.check();
        if (attributes.isEmpty()) {
            throw new IllegalStateException("Character should have one attribute at least");
        }
        if (!attributes.stream().filter(attr -> attr.getTrait().equals(Trait.NAME)).findFirst().isPresent()) {
            throw new IllegalStateException("Character should have a name");
        }
        hasBuilt = true;
        return new CharacterImpl(attributes);
    }

    private void check() {
        if (hasBuilt) {
            throw new IllegalStateException("Has already been built");
        }
    }

}
