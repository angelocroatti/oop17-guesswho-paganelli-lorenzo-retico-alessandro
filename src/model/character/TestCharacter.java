package model.character;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.Test;
import model.attribute.Attribute;
import model.attribute.AttributeFactory;
import model.attribute.feature.PhysicalFeature;
import model.attribute.feature.Trait;

class TestCharacter {

    private static final String WRONG = "Assertion not true";

    @Test
    public void testCharacter() {
        final Attribute rufyHairs = AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK, PhysicalFeature.Length.SHORT);
        //assertThrows(IllegalArgumentException.class, () -> new CharacterImpl("null", null));
        final Character rufy = new CharacterImpl(new HashSet<>(Arrays.asList(rufyHairs, 
                AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.MALE), AttributeFactory.from(Trait.HAT), 
                AttributeFactory.from(Trait.NAME, "Rufy"))));
        assertTrue(rufy.has(rufyHairs), WRONG);
        assertTrue(rufy.has(AttributeFactory.from(Trait.HAIRS, PhysicalFeature.Color.BLACK)), WRONG);
        assertTrue(rufy.has(AttributeFactory.from(Trait.HAT)), WRONG);
        assertTrue(rufy.has(AttributeFactory.from(Trait.GENDER, PhysicalFeature.Gender.MALE)), WRONG);
        assertFalse(rufy.has(AttributeFactory.from(Trait.GLASSES)), "Assertion was true");
        System.out.println("Test Character" + System.lineSeparator() + rufy + System.lineSeparator());
    }

}
