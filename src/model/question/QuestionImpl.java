package model.question;

import java.util.Objects;
import model.attribute.Attribute;
import model.attribute.feature.Trait;
import utilities.Messages;
import utilities.ToString;
import utilities.Utilities;

class QuestionImpl implements Question {

    private final Attribute attribute;

    QuestionImpl(final Attribute attribute) {
        Utilities.requireNonNull(attribute);
        this.attribute = attribute;
    }

    @Override
    public Attribute toAttribute() {
        return attribute;
    }

    @Override
    public String toString() {
        final String q;
        switch (attribute.getTrait()) {
            case NAME : q = Messages.CHARACTER_QUESTION_PREFIX + attribute.toString().replace(Trait.NAME.toString(), ""); break;
            case GENDER : q = Messages.GENDER_QUESTION_PREFIX + attribute.toString().replace(Trait.GENDER.toString(), ""); break;
            default : q = Messages.QUESTION_PREFIX + this.getArticle() + attribute.toString();
        }
        return q + Messages.QUESTION_SUFFIX;
    }

    @Override
    public int hashCode() {
        return Objects.hash(attribute);
    }

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Question ? ((Question) obj).toAttribute().equals(attribute) : false;
    }

    private String getArticle() {
        try {
            return attribute.getTrait().getClass().getField(attribute.getTrait().name()).isAnnotationPresent(ToString.class) 
                    ? attribute.getTrait().getClass().getField(attribute.getTrait().name()).getAnnotation(ToString.class).article() + " "
                    : "";
        } catch (NoSuchFieldException | SecurityException ingored) {
            return "";
        }
    }

}
